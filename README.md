#Python Currency Service

This piece of code is a Flask app written in Python. The purpose of the app is to consume, and parse, the copy of the daily currency conversion values provided for free from the European Central Bank at the following URL:

http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml

There are many currency conversion API's available that are available for public consumption, but those cost money to do at scale, and we're cheap. 

##Current Functionality:

Runs via Flask on 5000

Accepts URL input, Converts to foreign currency based on parsed XML. Prints and Returns value from function. 

##Working On:

K8s Manifest
